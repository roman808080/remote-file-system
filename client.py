from socket_wrapper import Socket
from file_system import RemoteFileSystem


def main():
    socket = Socket('127.0.0.1', 50008)
    remote_file_system = RemoteFileSystem(socket=socket)
    result = remote_file_system.create_directory('directory')
    print(result)

    new_file = remote_file_system.open_file('new_file', 'w')
    print(new_file)

    new_file.write('hell world')


if __name__ == '__main__':
    main()
