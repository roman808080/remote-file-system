import json
import socket
import unittest
from threading import Thread
from unittest.mock import Mock, patch, call, mock_open

from file_system import RemoteFileSystem, FileSystem
from file_system import return_exception_from_file_system
import socket_wrapper
from socket_wrapper import Socket, READ_BYTES, Server, MAX_CONNECTIONS
import callback_handler
from callback_handler import CallbackHandler
from stream import Stream, RemoteStream


class TestRemoteFileSystem(unittest.TestCase):
    def test_remove(self):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps({'result': 'Done'}).encode()

        remote_file_system = RemoteFileSystem(mock_socket)
        result = remote_file_system.remove('path/to/file')

        self.assertEqual(result, 'Done')
        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list  # return [call(*args, **kwargs)]
        call_object = call_objects[0]  # return call(*args, **kwargs)
        args = call_object[0]  # return *args
        kwargs = call_object[1]  # return **kwargs

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'file_system', 'kwargs': {}, 'function': 'remove', 'args': ['path/to/file']})

        self.assertEqual(len(kwargs), 0)

    def test_create_directory(self):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps({'result': 'Done'}).encode()

        remote_file_system = RemoteFileSystem(mock_socket)
        result = remote_file_system.create_directory('path/to/directory')

        self.assertEqual(result, 'Done')
        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list  # return [call(*args, **kwargs)]
        call_object = call_objects[0]  # return call(*args, **kwargs)
        args = call_object[0]  # return *args
        kwargs = call_object[1]  # return **kwargs

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'file_system','kwargs': {}, 'function': 'create_directory',
                              'args': ['path/to/directory']})

        self.assertEqual(len(kwargs), 0)

    def test_enumerate_dir(self):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps({'result': 'Done'}).encode()

        remote_file_system = RemoteFileSystem(mock_socket)
        result = remote_file_system.enumerate_dir('path/to/directory')

        self.assertEqual(result, 'Done')
        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list  # return [call(*args, **kwargs)]
        call_object = call_objects[0]  # return call(*args, **kwargs)
        args = call_object[0]  # return *args
        kwargs = call_object[1]  # return **kwargs

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'file_system', 'kwargs': {}, 'function': 'enumerate_dir',
                              'args': ['path/to/directory']})

        self.assertEqual(len(kwargs), 0)

    @patch('file_system.RemoteStream', return_value=Mock())
    def test_open_file(self, stream_remote_stream):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps(
            {'result': 'test_hash'}).encode()

        remote_file_system = RemoteFileSystem(mock_socket)
        result = remote_file_system.open_file('path/to/file', 'r')

        self.assertEqual(result, stream_remote_stream.return_value)
        self.assertEqual(stream_remote_stream.call_args_list, [call(hash='test_hash', socket=mock_socket)])

        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list
        call_object = call_objects[0]
        args = call_object[0]
        kwargs = call_object[1]

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'file_system', 'kwargs': {}, 'function': 'open_file',
                              'args': ['path/to/file', 'r']})

        self.assertEqual(len(kwargs), 0)

    def test_exception_handler(self):
        def function_without_exception(first_argument):
            return 'success'

        function_in_wrapper = return_exception_from_file_system(function_without_exception)
        self.assertEqual(function_in_wrapper(None), 'success')

    def test_exception_handler_raise_exception(self):
        def function_raise_exception(first_argument):
            raise Exception('Test exception')

        function_in_wrapper = return_exception_from_file_system(function_raise_exception)
        self.assertEqual(function_in_wrapper(None), repr(Exception('Test exception')))


class TestSocket(unittest.TestCase):
    @patch('socket.socket')
    def test_async_socket(self, MockSocketClass):

        mock_thread = Mock()
        socket_wrapper = Socket('localhost', 80, run_async=mock_thread)

        self.assertEqual(MockSocketClass.call_count, 1)
        self.assertEqual(MockSocketClass.call_args, call(socket.AF_INET, socket.SOCK_STREAM))

        self.assertEqual(mock_thread.call_count, 1)
        self.assertEqual(mock_thread.call_args, call(target=socket_wrapper._run, daemon=True))

        magic_mock_socket = MockSocketClass.return_value
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80))])

        socket_wrapper.write(b'bytes')
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80)), call.sendall(b'bytes')])

        socket_wrapper.read()
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80)), call.sendall(b'bytes'),
                                                          call.recv(READ_BYTES)])

    @patch('socket.socket')
    def test_sync_socket(self, MockSocketClass):
        socket_wrapper = Socket('localhost', 80)

        self.assertEqual(MockSocketClass.call_count, 1)
        self.assertEqual(MockSocketClass.call_args, call(socket.AF_INET, socket.SOCK_STREAM))

        magic_mock_socket = MockSocketClass.return_value
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80))])

        socket_wrapper.write(b'bytes')
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80)), call.sendall(b'bytes')])

        socket_wrapper.read()
        self.assertEqual(magic_mock_socket.method_calls, [call.connect(('localhost', 80)), call.sendall(b'bytes'),
                                                          call.recv(READ_BYTES)])


class TestCallbackHandler(unittest.TestCase):
    def test_callback_handler_file_system_object(self):
        mock_file_system = Mock()
        mock_file_system.remove.return_value = 'Success'
        callback_handler = CallbackHandler(file_system=mock_file_system)

        mock_socket = Mock()
        data = json.dumps({'object': 'file_system', 'function': 'remove',
                           'args': ['path/to/file'], 'kwargs': {}}).encode()
        callback_handler(socket=mock_socket, data=data)

        self.assertEqual(mock_socket.method_calls,
                         [call.write(b'{"result": "Success"}')])

    def test_callback_handler_raise_exception(self):
        mock_file_system = Mock()
        mock_file_system.remove.return_value = 'Success'
        callback_handler = CallbackHandler(file_system=mock_file_system)

        mock_socket = Mock()
        mock_socket.write.side_effect = Exception('Test exception')
        data = json.dumps({'object': 'file_system', 'function': 'remove',
                           'args': ['path/to/file'], 'kwargs': {}}).encode()

        with self.assertRaises(Exception) as context:
            callback_handler(socket=mock_socket, data=data)()
        self.assertEqual('Test exception', str(context.exception))

    @patch('callback_handler.is_stream', return_value=True)
    def test_callback_handler_stream_object(self, builtins_type):
        mock_file_system = Mock()
        callback_handler = CallbackHandler(file_system=mock_file_system)

        mock_socket = Mock()
        data = json.dumps({'object': 'file_system',
                           'function': 'create_directory',
                           'args': ['path/to/file', 'r'],
                           'kwargs': {}}).encode()
        callback_handler(socket=mock_socket, data=data)

        mock_stream_object = mock_file_system.create_directory.return_value
        mock_stream_hash = str(mock_stream_object)

        result = json.dumps({'result': mock_stream_hash}).encode()
        self.assertEqual(mock_socket.method_calls, [call.write(result)])

        mock_socket = Mock()
        data = json.dumps({'object': 'stream',
                           'hash': mock_stream_hash,
                           'function': 'read',
                           'args': [4],
                           'kwargs': {}}).encode()
        mock_stream_object.read.return_value = 'text'
        callback_handler(socket=mock_socket, data=data)

        result = json.dumps({'result': 'text'}).encode()
        self.assertEqual(mock_socket.method_calls, [call.write(result)])


class TestStream(unittest.TestCase):
    @patch('builtins.open', new_callable=mock_open, read_data=b'data')
    def test_stream_read(self, builtins_open):
        stream = Stream('some.txt', 'r')

        self.assertEqual(builtins_open.call_count, 1)
        self.assertEqual(builtins_open.call_args, call('some.txt', 'r'))

        self.assertEqual(stream.read(10), b'data')

        magic_mock_file = builtins_open.return_value
        self.assertEqual(magic_mock_file.method_calls, [call.read(10)])

    @patch('builtins.open', new_callable=mock_open)
    def test_stream_write(self, builtins_open):
        stream = Stream('some.txt', 'w')

        self.assertEqual(builtins_open.call_count, 1)
        self.assertEqual(builtins_open.call_args, call('some.txt', 'w'))

        stream.write(b'bytes')

        magic_mock_file = builtins_open.return_value
        self.assertEqual(magic_mock_file.method_calls, [call.write(b'bytes')])


class TestFileSystem(unittest.TestCase):
    @patch('shutil.rmtree')
    @patch('os.path.isdir', return_value=True)
    def test_file_system_remove_directory(self, os_path_isdir, shutil_rmtree):
        file_system = FileSystem()
        file_system.remove('path/to/directory')

        self.assertEqual(os_path_isdir.call_count, 1)
        self.assertEqual(os_path_isdir.mock_calls, [call('path/to/directory')])

        self.assertEqual(shutil_rmtree.call_count, 1)
        self.assertEqual(shutil_rmtree.mock_calls, [call('path/to/directory')])

    @patch('os.remove')
    @patch('os.path.isdir', return_value=False)
    def test_file_system_remove_file(self, os_path_isdir, os_remove):
        file_system = FileSystem()
        file_system.remove('path/to/file')

        self.assertEqual(os_path_isdir.call_count, 1)
        self.assertEqual(os_path_isdir.mock_calls, [call('path/to/file')])

        self.assertEqual(os_remove.call_count, 1)
        self.assertEqual(os_remove.mock_calls, [call('path/to/file')])

    @patch('os.makedirs')
    @patch('os.path.exists', return_value=False)
    def test_file_system_create_directory(self, os_path_exists, os_makedirs):
        file_system = FileSystem()
        file_system.create_directory('path/to/new/directory')

        self.assertEqual(os_path_exists.call_count, 1)
        self.assertEqual(os_path_exists.mock_calls,
                         [call('path/to/new/directory')])

        self.assertEqual(os_makedirs.call_count, 1)
        self.assertEqual(os_makedirs.mock_calls,
                         [call('path/to/new/directory')])

    @patch('os.makedirs')
    @patch('os.path.exists', return_value=True)
    def test_file_system_create_directory_exist(self, os_path_exists, os_makedirs):
        file_system = FileSystem()
        file_system.create_directory('path/to/new/directory')

        self.assertEqual(os_path_exists.call_count, 1)
        self.assertEqual(os_path_exists.mock_calls,
                         [call('path/to/new/directory')])

        self.assertEqual(os_makedirs.call_count, 0)

    @patch('os.listdir', return_value=[])
    def test_enumerate_dir(self, os_listdir):
        file_system = FileSystem()
        self.assertEqual(file_system.enumerate_dir('path/to/directory'), [])

        self.assertEqual(os_listdir.call_count, 1)
        self.assertEqual(os_listdir.mock_calls, [call('path/to/directory')])

    @patch('builtins.open', new_callable=mock_open)
    def test_open_file(self, builtins_open):
        file_system = FileSystem()
        self.assertIsInstance(file_system.open_file('path/to/file', 'r'), Stream)


class TestCallbackExceptionHandler(unittest.TestCase):
    def test_return_exception(self):
        def function_without_exception(self, socket, data):
            return 'success'

        mock_socket = Mock()
        function_in_wrapper = callback_handler.return_exception(function_without_exception)
        self.assertEqual(function_in_wrapper(self=None, socket=mock_socket, data=None), 'success')

    def test_return_exception_raise_exception(self):
        def function_with_exception(self, socket, data):
            raise Exception('Test exception')

        mock_socket = Mock()
        function_in_wrapper = callback_handler.return_exception(function_with_exception)

        function_in_wrapper(self=None, socket=mock_socket, data=None)

        param = json.dumps({'exception': repr(Exception('Test exception'))}).encode()
        self.assertEqual(mock_socket.write.call_args_list, [call(param)])


class TestServer(unittest.TestCase):
    @patch('socket_wrapper.Socket')
    @patch('socket.socket')
    def test_server(self, socket_socket, socket_wrapper_socket):
        mock_callback = Mock()
        mock_socket = socket_socket.return_value

        server = Server(host='host', port=80, callback=mock_callback)
        self.assertEqual(socket_socket.call_args_list, [call(socket.AF_INET, socket.SOCK_STREAM)])
        self.assertEqual(mock_socket.method_calls, [call.bind(('host', 80)), call.listen(MAX_CONNECTIONS)])

        mock_connection = Mock()
        mock_socket.accept.return_value = (mock_connection, None)

        server.wait_until_accept()

        self.assertTrue(mock_socket.accept.called)
        self.assertEqual(socket_wrapper_socket.call_args_list, [call(connection=mock_connection, run_async=Thread,
                                                                     callback=mock_callback)])


class TestSocketWrapperExceptionHandler(unittest.TestCase):
    def test_exception_handler(self):
        def function_without_exception(first_argument):
            return 'success'

        function_in_wrapper = socket_wrapper.handle_exception(function_without_exception)
        self.assertEqual(function_in_wrapper(None), 'success')

    @patch('log_manager.LOG.error')
    def test_exception_handler_raise_exception(self, log_error):
        def function_raise_exception(first_argument):
            raise Exception('Test exception')

        function_in_wrapper = socket_wrapper.handle_exception(function_raise_exception)
        self.assertEqual(function_in_wrapper(None), None)
        self.assertEqual(log_error.call_args_list, [call(repr(Exception('Test exception')))])


class TestRemoteStream(unittest.TestCase):
    def test_read(self):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps({'result': 'Done'}).encode()

        remote_stream = RemoteStream(socket=mock_socket, hash='test_hash')
        result = remote_stream.read(4)

        self.assertEqual(result, 'Done')
        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list
        call_object = call_objects[0]
        args = call_object[0]
        kwargs = call_object[1]

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'stream', 'hash': 'test_hash',
                              'kwargs': {}, 'function': 'read', 'args': [4]})

        self.assertEqual(len(kwargs), 0)

    def test_write(self):
        mock_socket = Mock()
        mock_socket.read.return_value = json.dumps({'result': 'Done'}).encode()

        remote_stream = RemoteStream(socket=mock_socket, hash='test_hash')
        result = remote_stream.write('text')

        self.assertEqual(result, 'Done')
        self.assertEqual(mock_socket.write.call_count, 1)
        self.assertEqual(mock_socket.read.call_count, 1)

        call_objects = mock_socket.write.call_args_list
        call_object = call_objects[0]
        args = call_object[0]
        kwargs = call_object[1]

        self.assertEqual(len(args), 1)
        self.assertDictEqual(json.loads(args[0].decode()),
                             {'object': 'stream', 'hash': 'test_hash',
                              'kwargs': {}, 'function': 'write',
                              'args': ['text']})

        self.assertEqual(len(kwargs), 0)
