from socket_wrapper import Server
from callback_handler import CallbackHandler
from file_system import FileSystem


def main():
    file_system = FileSystem()
    callback_handler = CallbackHandler(file_system=file_system)

    server = Server('0.0.0.0', 50008, callback_handler)
    server.start_server()


if __name__ == '__main__':
    main()
