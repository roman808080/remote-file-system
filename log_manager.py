class LOG:
    _log_function = None

    @staticmethod
    def error(message):
        LOG._log_function(message)

    @staticmethod
    def set_log_function(log_function):
        LOG._log_function = log_function


LOG.set_log_function(print)
