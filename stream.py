from abc import abstractmethod
import json


class IStream:
    @abstractmethod
    def read(self, bytes_count):
        pass

    @abstractmethod
    def write(self, write_bytes):
        pass


class Stream(IStream):
    def __init__(self, path, mode):
        self._file = open(path, mode)

    def read(self, bytes_count):
        return self._file.read(bytes_count)

    def write(self, write_bytes):
        self._file.write(write_bytes)


class RemoteStream(IStream):
    def __init__(self, socket, hash):
        self._socket = socket
        self._hash = hash

    def read(self, bytes_count):
        return self._call_remote_function('read', bytes_count)

    def write(self, write_bytes):
        return self._call_remote_function('write', write_bytes)

    def _call_remote_function(self, method_name, *args, **kwargs):
        encode_text = json.dumps({'function': method_name, 'args': args,
                                  'kwargs': kwargs, 'object': 'stream',
                                  'hash': self._hash}).encode()
        self._socket.write(encode_text)

        return json.loads(self._socket.read().decode())['result']
