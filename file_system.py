from abc import abstractmethod
from stream import IStream, Stream, RemoteStream
import json
import shutil
import os
from log_manager import LOG


def return_exception_from_file_system(function_obj):
    def wrapper(self, *args, **kwargs):
        try:
            return function_obj(self, *args, **kwargs)
        except Exception as exc:
            LOG.error(repr(exc))
            return repr(exc)
    return wrapper


class IFileSystem:
    @abstractmethod
    def remove(self, path):
        pass

    @abstractmethod
    def create_directory(self, path):
        pass

    @abstractmethod
    def enumerate_dir(self, path) -> []:
        pass

    @abstractmethod
    def open_file(self, path, mode) -> IStream:
        pass


class FileSystem(IFileSystem):
    @return_exception_from_file_system
    def remove(self, path):
        if os.path.isdir(path):
            return shutil.rmtree(path)
        else:
            return os.remove(path)

    @return_exception_from_file_system
    def create_directory(self, path):
        if not os.path.exists(path):
            os.makedirs(path)

    @return_exception_from_file_system
    def enumerate_dir(self, path):
        return os.listdir(path)

    @return_exception_from_file_system
    def open_file(self, path, mode):
        return Stream(path, mode)


class RemoteFileSystem(IFileSystem):
    def __init__(self, socket):
        self._socket = socket

    def remove(self, path):
        return self._call_remote_function('remove', path)

    def create_directory(self, path):
        return self._call_remote_function('create_directory', path)

    def enumerate_dir(self, path):
        return self._call_remote_function('enumerate_dir', path)

    def open_file(self, path, mode):
        return RemoteStream(socket=self._socket,
                            hash=self._call_remote_function('open_file',
                                                            path, mode))

    def _call_remote_function(self, method_name, *args, **kwargs):
        encode_text = json.dumps({'function': method_name,
                                  'args': args, 'kwargs': kwargs,
                                  'object': 'file_system'}).encode()
        self._socket.write(encode_text)

        return json.loads(self._socket.read().decode())['result']
