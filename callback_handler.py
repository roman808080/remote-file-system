import json
from log_manager import LOG
from stream import Stream


def return_exception(function_obj):
    def wrapper(self, socket, data):
        try:
            return function_obj(self, socket, data)
        except Exception as exc:
            LOG.error(repr(exc))

            exc_response = json.dumps({'exception': repr(exc)})
            socket.write(exc_response.encode())
    return wrapper


def is_stream(obj):
    return type(obj) is Stream


class CallbackHandler:
    def __init__(self, file_system):
        self._file_system = file_system
        self._streams = {}

    @return_exception
    def __call__(self, socket, data):
        parameters = json.loads(data.decode())
        response = None

        if parameters['object'] == 'file_system':
            response = getattr(self._file_system,
                               parameters['function'])(*parameters['args'],
                                                       **parameters['kwargs'])
        elif parameters['object'] == 'stream':
            response = getattr(self._streams[parameters['hash']],
                               parameters['function'])(*parameters['args'],
                                                       **parameters['kwargs'])

        if is_stream(response):
            self._streams[str(response)] = response

        result = json.dumps({'result': str(response)})
        socket.write(result.encode())
