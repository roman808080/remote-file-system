from abc import abstractmethod
from threading import Thread
import socket
from log_manager import LOG


MAX_CONNECTIONS = 100
READ_BYTES = 1024


def handle_exception(function_obj):
    def wrapper(self, *args, **kwargs):
        try:
            return function_obj(self, *args, **kwargs)
        except Exception as exc:
            LOG.error(repr(exc))
            return None
    return wrapper


class ISocket:
    @abstractmethod
    def write(self, bytes):
        pass

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def set_on_read(self, on_read_callback):
        pass

    @abstractmethod
    def close(self):
        pass


class IServer:
    @abstractmethod
    def start_server(self):
        pass


class Socket(ISocket):
    def __init__(self, host=None, port=None, connection=None, run_async=None, callback=None):
        self._thread = None

        if callback:
            self._on_read_callback = callback
        else:
            self._on_read_callback = lambda client_socket, data: print(data)

        if connection:
            self._socket = connection
        else:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect((host, port))

        self._run_async = run_async
        if self._run_async:
            self._thread = self._run_async(target=self._run, daemon=True)
            self._thread.start()

    @handle_exception
    def _run(self):
        while True:
            self.async_receive()

    def async_receive(self):
        data = self._socket.recv(READ_BYTES)
        self._on_read_callback(self, data)

    @handle_exception
    def write(self, bytes):
        self._socket.sendall(bytes)

    @handle_exception
    def read(self):
        return self._socket.recv(READ_BYTES)

    @handle_exception
    def set_on_read(self, on_read_callback):
        self._on_read_callback = on_read_callback

    @handle_exception
    def close(self):
        self._socket.close()


class Server(IServer):
    def __init__(self, host, port, callback):
        self._client_sockets = []

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._callback = callback

        self._socket.bind((host, port))
        self._socket.listen(MAX_CONNECTIONS)

    def start_server(self):
        while True:
            self.wait_until_accept()

    def wait_until_accept(self):
        connection, _ = self._socket.accept()
        client_socket = Socket(connection=connection, run_async=Thread, callback=self._callback)
        self._client_sockets.append(client_socket)
